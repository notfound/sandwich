
package provider;

import control.KeyGenerator;
import io.jsonwebtoken.Jwts;
import java.io.IOException;
import java.security.Key;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthentificationFiltre implements ContainerRequestFilter{
    
@Inject
private KeyGenerator keygenerator;

@Override
public void filter(ContainerRequestContext requestContext) throws IOException{
    
        String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authHeader == null || !authHeader.startsWith("Bearer")){
            throw new NotAuthorizedException("Problème header autorisation");
          
        }
        
        String token = authHeader.substring("Bearer".length()).trim();
        
        try{
            Key cle = keygenerator.generateKey();
            Jwts.parser().setSigningKey(cle).parseClaimsJws(token);
            System.out.println("token valide");
        } catch (Exception e){
            System.out.println("Token invalide");
            requestContext
                    .abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
        }
}

