/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordManagement {
    
    public static String digestPass(String plainText){
        try{
            String hashed = BCrypt.hashpw(plainText, BCrypt.gensalt());
            System.out.println("hashed"+ hashed);
            return hashed;
        }catch (Exception e){
            throw new RuntimeException("pb hash de mot de passe", e);
        }
        
    }
    
}
