package boundary;

import entity.Categorie;
import entity.Ingredient;
import java.net.URI;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/categories")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class CategorieRepresentation {

    @EJB
    CategorieResource catResource;
    @EJB
    IngredientResource ingredientResource;

    @GET
    public Response getCategories(@Context UriInfo uriInfo) {
        List<Categorie> liste = this.catResource.findAll();        
        liste.stream().forEach(c -> {
            c.setIngredients(this.ingredientResource.findAll(c.getId()));
            List<Ingredient> li = c.getIngredients();
            li.stream().forEach(i->{
                i.getLinks().clear();
                i.addLink(this.getUriForSelfIngredient(uriInfo, i, c), "self"); 
            });        
            c.getLinks().clear();
            c.addLink(this.getUriForSelfCategorie(uriInfo, c), "self");
        });
        GenericEntity<List<Categorie>> list = new GenericEntity<List<Categorie>>(liste) {
        };
        return Response.ok(list, MediaType.APPLICATION_JSON).build();
    }
    
        @GET
        @Path("/{id}")
    public Response getCategories(@PathParam("id")String id ,@Context UriInfo uriInfo) {
        Categorie c = this.catResource.findById(id);
        c.setIngredients(this.ingredientResource.findAll(id));
        return Response.ok(c, MediaType.APPLICATION_JSON).build();
    }
    
    @PUT
    @Path("/{id}")
    public Response modifierCategorie(@PathParam("id")String id, Categorie c){
        Categorie nc = this.catResource.findById(id);
        nc.setNom(c.getNom());
        nc.setIngredients(this.ingredientResource.findAll(id));
         return Response.ok(nc, MediaType.APPLICATION_JSON).build();
    }

    @POST
    public Response addCategorie(Categorie categorie, @Context UriInfo uriInfo) {
        Categorie newCategorie = this.catResource.save(categorie);
        URI uri = uriInfo.getAbsolutePathBuilder().path(newCategorie.getId()).build();
        return Response.created(uri)
                .entity(newCategorie)
                .build();
    }
    
    @POST
    @Path("/{id}/ingredients")
    public Response addIngredientForCategorie(@PathParam("id")String id, Ingredient i){
        this.ingredientResource.ajouteIngredient(id, i);
        Categorie c = this.catResource.findById(id);
        c.setIngredients(this.ingredientResource.findAll(id));
        return Response.ok(c).build();
    }
    
    @DELETE
    @Path("/{categorieId}")
    public Response deleteCategorie(@PathParam("categorieId") String id, @Context UriInfo uriInfo) {
        this.catResource.delete(id);
        return this.getCategories(uriInfo);
    }
    
    @DELETE
    @Path("/{categorieId}/ingredients/{ingid}")
    public Response deleteIngredientCategorie(@PathParam("categorieId")String Id, @PathParam("ingid")String ingId, @Context UriInfo uriInfo){
        this.ingredientResource.removeIngredient(ingId);
        return this.getCategories(Id, uriInfo);
    }

    private String getUriForSelfCategorie(UriInfo uriInfo, Categorie c){
        String uri = uriInfo.getBaseUriBuilder()
                .path(CategorieRepresentation.class)
                .path(c.getId())
                .build()
                .toString();
        return uri;
    }
    

    private String getUriForCategorie(UriInfo uriInfo){
        String uri = uriInfo.getBaseUriBuilder()
                .path(CategorieRepresentation.class)  // va rechercher @path messages
                .build()
                .toString();
        return uri;
    }
    
    // pour un ingredient d'une categorie
    private String getUriForSelfIngredient(UriInfo uriInfo, Ingredient i, Categorie c){
        String uri = uriInfo.getBaseUriBuilder()
                .path(CategorieRepresentation.class)
                .path(c.getId())
                .path(IngredientRepresentation.class)
                .path(i.getId())
                .build()
                .toString();
        return uri;
    }
    
    // pour la collection d'ingredient d'une categorie
    private String getUriForIngredient(UriInfo uriInfo, Categorie c){
        String uri = uriInfo.getBaseUriBuilder()
                .path(CategorieRepresentation.class)
                .path(c.getId())
                .path(IngredientRepresentation.class)
                .build()
                .toString();
        return uri;
    }    

}
