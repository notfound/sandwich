package boundary;

import entity.Ingredient;
import java.net.URI;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/ingredients")
@Stateless
public class IngredientRepresentation {

    @EJB
    IngredientResource ingredientResource;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{ingredientId}")
    public Response getIngredients(@PathParam("ingredientId") String ingredientId) {
        Ingredient i = this.ingredientResource.findById(ingredientId);
        if (i != null) {
            return Response.ok(i).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
     @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIngredients() {
         List<Ingredient> li = this.ingredientResource.findAll();
        if (li != null) {
            return Response.ok(li).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Path("/{id}")
    public Response removeIngredient(@PathParam("id") String id){
        this.ingredientResource.removeIngredient(id);
        return Response.ok().build();
    }
    
       @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response creerIngredient(Ingredient ingredient) {
        Ingredient i = this.ingredientResource.save(ingredient);
        System.out.println(i.getId());
        return Response.ok(i).build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response modifier(@PathParam("id") String id, Ingredient in){
        Ingredient i = this.ingredientResource.findById(id);
        i.setNom(in.getNom());
        return Response.ok(i).build();
        
    }
    

 
}
