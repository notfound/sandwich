package boundary;

import entity.Categorie;
import entity.Ingredient;
import entity.Sandwich;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class IngredientResource {

    @PersistenceContext
    EntityManager em;

    public Ingredient findById(String id) {
        return this.em.find(Ingredient.class, id);
    }

    public List<Ingredient> findAll(String categorieId) {
        Query query = em.createQuery("SELECT i FROM Ingredient i where i.categorie.id= :id ");
        query.setParameter("id", categorieId);
        List<Ingredient> liste = query.getResultList();
        return liste;
    }
    
    public Ingredient save(Ingredient i){
         i.setId(UUID.randomUUID().toString());
        
        return this.em.merge(i);
    }
    
    
      public List<Ingredient> findAll() {
        Query query = em.createQuery("SELECT i FROM Ingredient i  ");
        List<Ingredient> liste = query.getResultList();
        return liste;
    }
    
    public List<Ingredient> findIngredientForSandwich(String id){
        Query query = em.createQuery("SELECT i FROM Ingredient i where i.sandwich.id= :id ");
        query.setParameter("id", id);
        List<Ingredient> liste = query.getResultList();
        return liste;
    }
    

    public Ingredient ajouteIngredient(String catId, Ingredient ingredient) {
        Ingredient i;
        if(ingredient.getId()==null){
            i = new Ingredient(ingredient.getNom());
            i.setId(UUID.randomUUID().toString());
            i.setCategorie(this.em.find(Categorie.class, catId));
            this.em.merge(i);
        }else{
            i = this.em.find(Ingredient.class, ingredient.getId());
            
        }
       i.setCategorie(this.em.find(Categorie.class, catId));
        return i;
    }
    
    public Ingredient ajouterIngredientSandwich(String id, Ingredient in){
        Ingredient i;
    
        if(in.getId()==null){
                System.out.println(" lsjghfsggfkjgh :"+in.getId());
            i  = new Ingredient(in.getNom());
            i.setId(UUID.randomUUID().toString());
              i.setSandwich(this.em.find(Sandwich.class, id));
             this.em.merge(i);
        }else{
            i = this.em.find(Ingredient.class, in.getId());
        }
       
        i.setSandwich(this.em.find(Sandwich.class, id));

        return i;
    }
    
    public void removeIngredient (String id){
          try {
            Ingredient ref = this.em.getReference(Ingredient.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
    }
    
    public Ingredient modifierIngredient (String id, Ingredient ing){
        Ingredient i = this.em.find(Ingredient.class, id);
        if(ing.getNom()!=null){
            i.setNom(ing.getNom());
        }
        if(ing.getCategorie()!=null){
            i.setCategorie(ing.getCategorie());
        }
        return i;
    }

}
