/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entity.Categorie;
import entity.Commande;
import entity.Ingredient;
import entity.Sandwich;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Leopold
 */
@Stateless
public class CommandeResource {
    
       
    @PersistenceContext
    EntityManager em;
    
    public List<Commande> findAll(){
        Query q = this.em.createNamedQuery("Commande.findAll", Commande.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
    
    public Commande find(String id){
        return this.em.find(Commande.class, id);
    }
    
    public Commande save(Commande c){
        c.setId(UUID.randomUUID().toString());
        c.setSandwichs(new ArrayList<Sandwich> ());
        return this.em.merge(c);
    }
    
    public Commande addSandwich(String idCommande, Sandwich s){
        Commande c  = this.em.find(Commande.class, idCommande);
        if(s.getId()!=null){
                Sandwich ns = this.em.find(Sandwich.class, s.getId());
                if(ns != null){
                    ns.setCommande(c);
                    
                }                
        }else{
               Sandwich ns = new Sandwich();
                ns.setId(UUID.randomUUID().toString());
                System.out.println(ns.getId()+"  "+ns.getPain());
                if(s.getIngredients().size() != 0){
                     ns.setIngredients(s.getIngredients());
                }else{
                    ns.setIngredients(new ArrayList<Ingredient>());
                }
                ns.setPain(s.getPain());
                ns.setTaille(s.getTaille());  
                ns.setCommande(c);
                this.em.persist(ns);    
        }
        return c;     
    }
   
    
    public Commande removeSandwichs(String idCommande){
         Commande c  = this.em.find(Commande.class, idCommande);
         
         c.remmoveSandwichs();
         return c;
    }
    
    public void delete(String id){
          try {
            Commande ref = this.em.getReference(Commande.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
    }
    
    public Commande modifierEtat(String id, String etat){
        Commande ref = this.em.getReference(Commande.class, id);
        ref.setEtat(etat);
        return ref;
    }
    
    public Sandwich modifierSandwichCommande(String id, String idSandwich, Sandwich s){
        Commande c = this.em.find(Commande.class, id);
        Sandwich ms = this.em.find(Sandwich.class, idSandwich);
        if(s.getPain()!=null){
            ms.setPain(s.getPain());
        }
        if(s.getTaille()!=null){
            ms.setTaille(s.getTaille());
        }
        if(s.getIngredients().size()!=0){
            ms.setIngredients(s.getIngredients());
        }
        return ms;
    }
    
    public Commande modifierCommande(String id, Commande c){
        Commande mc = this.em.find(Commande.class, id);
        if(c.getMontant()!=0){
            mc.setMontant(c.getMontant());
        }
        if(c.getDate()!=null){
            mc.setDate(c.getDate());
        }
        if(c.getEtat()!=null){
            mc.setEtat(c.getEtat());
        }
        return mc;
        
    }
    
}
