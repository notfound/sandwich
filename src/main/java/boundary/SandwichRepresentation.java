/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entity.Categorie;
import entity.Ingredient;
import entity.Sandwich;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Leopold
 */

@Path("/sandwichs")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class SandwichRepresentation {
    
    @EJB
    SandwichResource sandResource;
    @EJB
    IngredientResource ingResource;
    
    @GET
    public Response getSandwichs(@Context UriInfo uriInfo){
        List<Sandwich> liste = this.sandResource.findAll();
           liste.stream().forEach(c -> {
            c.setIngredients(this.ingResource.findIngredientForSandwich(c.getId()));
            List<Ingredient> li = c.getIngredients();
           });
        
        GenericEntity<List<Sandwich>> list = new GenericEntity<List<Sandwich>>(liste) {
        };
        return Response.ok(list, MediaType.APPLICATION_JSON).build();
    }
    
      @GET
          @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
      @Path("/{id}")
    public Response getSandwichDetail(@PathParam("id")String id){
        Sandwich s = this.sandResource.find(id);
        s.setIngredients(this.ingResource.findIngredientForSandwich(id));
        
  
        return Response.ok(s, MediaType.APPLICATION_JSON).build();
    }
        
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSandwich(Sandwich sandwich, @Context UriInfo uriInfo) {
        Sandwich s = this.sandResource.save(sandwich);
        return Response.ok(sandwich).build();
    }    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response setSandwich(@PathParam("id")String id, Sandwich sandwich, @Context UriInfo uriInfo) {
        Sandwich s = this.sandResource.find(id);
        s.setPain(sandwich.getPain());
        s.setTaille(sandwich.getTaille());
        s.setIngredients(this.ingResource.findIngredientForSandwich(id));
        return Response.ok(s).build();
    }    
    
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/ingredients")
    public Response addIngredient(@PathParam("id")String id, Ingredient ing){
        Sandwich s = this.sandResource.find(id);
        this.ingResource.ajouterIngredientSandwich(id, ing);
        s.setIngredients(this.ingResource.findIngredientForSandwich(id));
        return Response.ok(s).build();
    }
    
    
      @DELETE 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/ingredients/{iding}")
    public Response removeIngredient(@PathParam("id")String id, @PathParam("iding")String iding){
        Sandwich s = this.sandResource.find(id);
        this.ingResource.removeIngredient(iding);
        s.setIngredients(this.ingResource.findIngredientForSandwich(id));
        return Response.ok(s).build();
    }
    
    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") String id, @Context UriInfo uriInfo) {
        this.sandResource.removeSandwich(id);
        return Response.ok().build();
    }    
    
}
