/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entity.Categorie;
import entity.Ingredient;
import entity.Sandwich;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Leopold
 */

@Stateless
public class SandwichResource {
        
    @PersistenceContext
    EntityManager em;
    
    public List<Sandwich> findAll(){        
         Query q = this.em.createNamedQuery("Sandwich.findAll", Sandwich.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
    
    public Sandwich find(String id){
        return this.em.find(Sandwich.class, id);
    }
    
    
    public Sandwich save( Sandwich s) {
        s.setId(UUID.randomUUID().toString());
        s.setIngredients(new ArrayList<Ingredient>());
        return this.em.merge(s);
    }
    
    
    public void removeSandwich (String id){
          try {
            Sandwich ref = this.em.getReference(Sandwich.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
    }

    public List<Sandwich> findSandwichsForCommande(String id) {
        Query q = this.em.createQuery("SELECT s FROM Sandwich s WHERE s.commande.id = :id ");
        q.setParameter("id", id);
        return q.getResultList();
    }

        
    
}
