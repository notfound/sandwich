/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import control.KeyGenerator;
import entity.Commande;
import entity.Ingredient;
import entity.Sandwich;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import provider.Secured;

/**
 *
 * @author Leopold
 */

@Path("/commandes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class CommandeRepresentation {
    
     @Inject
    private KeyGenerator keyManagement;
     
     @Context
    private UriInfo uriInfo;
    
    @EJB
    SandwichResource sandwichResource;
    @EJB
    CommandeResource commandeResource;
    @EJB
    IngredientResource ingredientResource;
    
     public String issueToken(String id){
        Key key = keyManagement.generateKey();
         String jwtToken = Jwts.builder()
                 .setSubject(id)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(5L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
          return jwtToken;
    }
     
       private Date toDate(LocalDateTime localDateTime) {
         return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    
    @GET
    public Response getAllCommandes(@Context UriInfo uriInfo){               
        List<Commande> lic = this.commandeResource.findAll();
        lic.stream().forEach(c->{
            c.setSandwichs(this.sandwichResource.findSandwichsForCommande(c.getId()));
        });        
        if (lic != null) {
            return Response.ok(lic).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getCommande(@PathParam("id") String id){
        Commande c = this.commandeResource.find(id);
         List<Sandwich> ls = this.commandeResource.find(id).getSandwichs();
        ls.stream().forEach(s->{
            
            s.setIngredients(this.ingredientResource.findIngredientForSandwich(s.getId()));
           
        });
        c.setSandwichs(ls);
           if (c!= null) {            
            return Response.ok(c).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs")
    public Response getSandwichsForCommande(@PathParam("id") String id){
        List<Sandwich> ls = this.commandeResource.find(id).getSandwichs();
        ls.stream().forEach(s->{
            
            s.setIngredients( this.ingredientResource.findIngredientForSandwich(s.getId() ));
           
        });
         return Response.ok(ls).build();
    }
    
    @POST
    public Response createCommande(Commande c, @Context UriInfo uriInfo ){
        Commande nc =  this.commandeResource.save(c);
        String token = issueToken(nc.getId());
        nc.setToken(token);
        return Response.ok(nc).header(AUTHORIZATION, "Bearer " + token).build();
    }
    
    @DELETE
    @Secured
    @Path("/{id}")
    public Response deleteCommande(@PathParam("id") String id, @Context UriInfo uriInfo ){
        this.commandeResource.delete(id);
        return this.getAllCommandes(uriInfo);
    }
    
    @PUT
    @Secured
     @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response modifierEtatCommande(@PathParam("id")String id, Commande c){
        Commande nc = this.commandeResource.modifierEtat(id, c.getEtat());
        List<Sandwich> ls = this.sandwichResource.findSandwichsForCommande(id);
        ls.stream().forEach(sa->{            
            sa.setIngredients(this.ingredientResource.findIngredientForSandwich(sa.getId()));
        });
        nc.setSandwichs(ls);
        
        return Response.ok().build();
    }
    
    @POST
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs")
    public Response ajouterSandwichCommande(@PathParam("id")String id, Sandwich s){
       Commande c= this.commandeResource.addSandwich(id, s);
       List<Sandwich> ls = this.sandwichResource.findSandwichsForCommande(id);
       ls.stream().forEach(sa->{            
            sa.setIngredients(this.ingredientResource.findIngredientForSandwich(sa.getId()));
        });
       c.setSandwichs(ls);
        return Response.ok(c).build();
    }
   
    
    @DELETE
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs/{idSandwich}")
    public Response supprSandwichInCommande(@PathParam("id")String id, @PathParam("idSandwich") String idSandwich){
        this.sandwichResource.removeSandwich(idSandwich);
        return this.getSandwichsForCommande(id);
    }
       
    
    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs/{idSandwich}")
    public Response modifierSandwichCommande(@PathParam("id")String id, @PathParam("idSandwich") String idSandwich, Sandwich s ){
        Sandwich ms = this.commandeResource.modifierSandwichCommande(id, idSandwich, s);
        ms.setIngredients(this.ingredientResource.findIngredientForSandwich(ms.getId()));
        return Response.ok(ms).build();
    }
    
    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response modifierCommande(@PathParam("id")String id, Commande c){
        Commande mc = this.commandeResource.modifierCommande(id, c);
        List<Sandwich> ls = this.sandwichResource.findSandwichsForCommande(id);
        ls.stream().forEach(sa->{            
            sa.setIngredients(this.ingredientResource.findIngredientForSandwich(sa.getId()));
        });
       mc.setSandwichs(ls);
        return  Response.ok(mc).build();
    }
    
     @PUT
     @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs/{idSandwich}/ingredients/{id}")
    public Response modifierIngredientSandwichCommande(@PathParam("id")String id, @PathParam("idSandwich") String idSandwich, @PathParam("idIng") String idIng, Ingredient ing ){
        Ingredient is = this.ingredientResource.modifierIngredient(idIng, ing);
        return Response.ok(is).build();
    }
    
    @POST
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs/{idSandwich}/ingredients")
    public Response ajouterIngredientSandwichCommande(@PathParam("id")String id, @PathParam("idSandwich") String idSandwich, Ingredient ing ){
        Ingredient is = this.ingredientResource.ajouteIngredient(idSandwich, ing);
        return Response.ok(is).build();
    }
    
    
    @DELETE
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/sandwichs/{idSandwich}/ingredients/{id}")
    public Response suppriIngredientSandwichCommande(@PathParam("id")String id, @PathParam("idSandwich") String idSandwich, @PathParam("idIng") String idIng, Ingredient ing ){
        this.ingredientResource.removeIngredient(idIng);
        Commande c = this.commandeResource.find(id);
        return Response.ok(c).build();
    }
   
    
    
    
}
