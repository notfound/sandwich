package entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

@Entity
public class Ingredient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private String nom;
    
    @ManyToOne
        @JsonBackReference(value="categorie")
    private Categorie categorie;
    @ManyToOne
    @JsonBackReference(value="sandwich")
    private Sandwich sandwich;

    public Sandwich getSandwich() {
        return sandwich;
    }

    public void setSandwich(Sandwich sandwich) {
        this.sandwich = sandwich;
    }
            
    @XmlElement(name="_links")
    @Transient 
     private List<Link> links = new ArrayList<>();
    
    public List<Link> getLinks(){
        return links;
    }
    
     public void addLink(String uri, String rel){
        this.links.add(new Link(rel, uri));
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }


    public Ingredient() {

    }

    public Ingredient(String nom) {
        this.nom = nom;
    }

    public Ingredient(String nom, Categorie c) {
        this.nom = nom;
        this.categorie = c;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
    

}
