package entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categorie.findAll", query = "SELECT c FROM Categorie c")
})
public class Categorie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String nom;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categorie")
    private List<Ingredient> ingredients;
    
    @XmlElement(name="_links")
    @Transient 
     private List<Link> links = new ArrayList<>();
    
    public List<Link> getLinks(){
        return links;
    }
    
     public void addLink(String uri, String rel){
        this.links.add(new Link(rel, uri));
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public Categorie() {
        this.ingredients = new ArrayList<>();
    }

    public Categorie(String id, String nom) {
        this.id = id;
        this.nom = nom;
        this.ingredients = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
