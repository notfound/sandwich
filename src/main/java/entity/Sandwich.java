package entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sandwich.findAll", query = "SELECT s FROM Sandwich s")
})
public class Sandwich implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String taille;    
    private String  pain;

    public String getPain() {
        return pain;
    }

    public void setPain(String pain) {
        this.pain = pain;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sandwich")
    private List<Ingredient> ingredients;
    
    @ManyToOne
    @JsonBackReference
    private Commande commande;

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
    
    @XmlElement(name="_links")
    @Transient 
     private List<Link> links = new ArrayList<>();
    
    public List<Link> getLinks(){
        return links;
    }
    
     public void addLink(String uri, String rel){
        this.links.add(new Link(rel, uri));
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public Sandwich() {
        this.ingredients = new ArrayList<>();

    }

    public Sandwich(String id) {
        this.id = id;
        this.ingredients = new ArrayList<>();
    }
    
    public Sandwich(String taille, String pain, List<Ingredient> ings){
        this.taille = taille;
        this.pain = pain;
        this.ingredients = ings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
    
       public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }
}
