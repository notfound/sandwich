/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Leopold
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c")
})
public class Commande implements Serializable {
     private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private Date date; 
    private String etat;
    private double montant;
    @Column(length=300)
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commande")
    private List<Sandwich> sandwichs;

    public List<Sandwich> getSandwichs() {
        return sandwichs;
    }

    public void setSandwichs(List<Sandwich> Sandwichs) {
        this.sandwichs = Sandwichs;
    }
     
    public Commande(){
        this.sandwichs = new ArrayList<Sandwich>();
        this.montant= 0;
        
       
    }
    
    public Commande(Date date, ArrayList<Sandwich> sandwichs){
        this.sandwichs= sandwichs;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void addSandwich(Sandwich s){
        this.sandwichs.add(s);
    }
    
    public void remmoveSandwichs(){
        this.sandwichs.clear();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
    
    public void setSandwichs(String id, Sandwich s){
        Iterator<Sandwich> iterator = this.sandwichs.iterator();
        int index = 0;
        while(iterator.hasNext()){
           Sandwich sIter = iterator.next();
           if(sIter.getId() != id){
              index ++;
           }else{
               break;
           }
        }
        this.sandwichs.set(index, s);
    }
    
}
